class Hangman
  attr_accessor :guesser, :referee, :board

  def initialize(options = {})
    default = {
      :guesser => nil,
      :referee => nil
    }
    options = default.merge(options)
    @guesser = options[:guesser]
    @referee = options[:referee]

  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def take_turn
    input = @guesser.guess
    @referee.check_guess(input)
    self.update_board(input,@referee.check_guess(input))
    @guesser.handle_response
  end

  def update_board(guess,inputs)
    inputs.each do |input|
      @board[input] = guess
    end

  end

end

class HumanPlayer
end

class ComputerPlayer
  attr_accessor :word_length

  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @secret_word = @dictionary.shuffle[0]
    @secret_word.length
  end

  def check_guess(letter)
    result = []
    @secret_word.chars.each_with_index do |char, index|
      if letter == char
        result.push(index)
      end
    end
    result
  end

  def register_secret_length(length)
    @word_length = length
  end

  def guess
    puts "Guesser, please enter a guess"
    input = gets.chomp
    input
  end

  def handle_response

  end

end
